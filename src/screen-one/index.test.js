import React from "react";
import { create } from "react-test-renderer";

import ScreenOne from ".";

jest.useFakeTimers();

jest.mock("@fortawesome/react-native-fontawesome", () => ({
  FontAwesomeIcon: "",
}));

describe("Screen One", () => {
  let wrapper;
  const fakeTodoData = [
    { text: "hello", isFinished: true },
    { text: "bye", isFinished: false },
    { text: "maybe", isFinished: false },
  ];

  beforeEach(async () => {
    wrapper = create(<ScreenOne />);
  });

  it("renders without error", () => {
    expect(wrapper.root.findByProps({ testID: "s1-container" })).toBeTruthy();
  });

  it("renders title without error", () => {
    const titleComponent = wrapper.root.findByProps({ testID: "s1-title" });
    expect(titleComponent.props.children.join("")).toEqual("Start Timer 0");
  });

  it("renders text input value according to what state variable text is set to", () => {
    wrapper.root.instance.setState({ text: "hello" });

    const textInputComponent = wrapper.root.findByProps({
      testID: "s1-text-input",
    });
    expect(textInputComponent.props.value).toEqual("hello");
  });

  it("renders correct number of todo list text components", () => {
    const root = wrapper.root;
    root.instance.setState({ items: fakeTodoData });
    const findAllByTestID = (root) =>
      root.findAll(
        (el) => el.props.testID === "s1-list-item-text" && el.type === "Text",
      );
    expect(findAllByTestID(root).length).toEqual(3);
  });

  it("renders correct number of todo list icons", () => {
    const root = wrapper.root;
    root.instance.setState({ items: fakeTodoData });
    const findAllByTestID = (root) =>
      root.findAll(
        (el) => el.props.testID === "s1-list-item-icon",
      );
    expect(findAllByTestID(root).length).toEqual(1);
  });
});
