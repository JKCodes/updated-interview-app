import React from "react";
import {
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faCheck, faMinusCircle } from "@fortawesome/free-solid-svg-icons";
import { Colors } from "../utils";
import styles from "./styles";

type ItemType = {
  text: string;
  isFinished: boolean;
}

type Props = {};
type State = {
  title: string;
  counter: number;
  items: ItemType[];
  text: string;
};

class ScreenOne extends React.Component<Props, State> {
  state: State = {
    title: "Start Timer",
    counter: 0,
    items: [],
    text: "",
  };

  timer: NodeJS.Timeout | undefined;

  componentDidMount() {
    this.timer = setInterval(() => {
      this.setState({ title: "Timer:", counter: this.state.counter + 1 });
    }, 1000);
  };

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  handleAddItem = () => {
    const {items, text} = this.state;
    const item = {
      text,
      isFinished: false,
    }

    this.setState({
      items: [
        ...items,
        item
      ],
      text: '',
    });
  };

  handleChangeText = (text: string) => {
    this.setState({ text });
  };

  handleItemTap = (index: number) => {
    const {items} = this.state;
    const item: ItemType = {
      ...items[index],
      isFinished: !items[index].isFinished
    }
    const updatedItems = [
      ...items.slice(0, index),
      item,
      ...items.slice(index + 1)
    ];

    this.setState({
      items: updatedItems,
    });
  }

  render() {
    return (
      <View 
        style={styles.container}
        testID="s1-container"
      >
        <Text 
          style={styles.title}
          testID="s1-title"
        >
          {this.state.title} {this.state.counter}
        </Text>

        <View 
          style={styles.headerContainer}
          testID="s1-header-container"
        >
          <Text style={styles.title}>
            Add New To-Do {StatusBar.currentHeight}
          </Text>
          <TextInput
            testID="s1-text-input"
            style={styles.input}
            value={this.state.text}
            placeholder="Enter To Do"
            onChangeText={this.handleChangeText}
          />
          <TouchableOpacity style={styles.submitToDoButton} onPress={this.handleAddItem}>
            <Text style={styles.buttonTitle}>Add To-Do</Text>
          </TouchableOpacity>
        </View>

        {this.state.items.map((item, index) => (
          <TouchableOpacity 
            style={styles.item} 
            key={Math.random()}
            onPress={() => this.handleItemTap(index)}  
          >
            {item.isFinished && <FontAwesomeIcon testID="s1-list-item-icon" icon={faCheck} size={15} color={Colors.blue} />}
            <Text testID="s1-list-item-text">
              {item.text}
            </Text>
            <FontAwesomeIcon
              icon={faMinusCircle}
              size={15}
              color={Colors.pink}
            />
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}

export default ScreenOne;
