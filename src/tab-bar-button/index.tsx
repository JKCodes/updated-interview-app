import React, { useCallback } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import styles from "./styles";

type TabButtonProps = {
  label: string;
  number: number;
  selected?: boolean;
  onPress?: (number: number) => void;
};

const TabBarButton = ({ number, label, selected, onPress }: TabButtonProps) => {
  const handlePress = useCallback(() => {
    if (onPress) {
      onPress(number);
    }
  }, [onPress, number]);

  return (
    <TouchableOpacity onPress={handlePress} style={styles.touchable}>
      <View style={[styles.button, selected && styles.selected]}>
        <Text style={[styles.number, selected && styles.selected]}>{number}</Text>
        <Text style={[styles.label, selected && styles.selected]}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default TabBarButton;
