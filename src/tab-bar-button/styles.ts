import { StyleSheet } from "react-native";
import { Colors } from "../utils"

const styles = StyleSheet.create({
  touchable: {
    flex: 1,
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
  },
  label: {
    fontSize: 12,
    fontWeight: "400",
  },
  number: {
    fontSize: 24,
    fontWeight: "700",
  },
  selected: {
    backgroundColor: Colors.black,
    color: Colors.white,
  }
});
export default styles;
