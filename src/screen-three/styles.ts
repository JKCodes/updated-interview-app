import { StyleSheet, Dimensions } from "react-native";
import { Colors } from "../utils";

const { width } = Dimensions.get("screen");

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.grey,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 106,
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    marginTop: 20,
    fontSize: 26,
    fontWeight: "600",
    textAlign: "center",
  },
  item: {
    margin: width * 0.025,
    width: width * 0.45,
  },
  image: {
    width: width * 0.45,
    height: width * 0.45,
    borderRadius: 5,
  },
  itemTextPrimary: {
    marginTop: 13,
    fontSize: 18.5,
    fontWeight: "600",
  },
  itemTextSecondary: {
    fontSize: 16,
    color: Colors.darkGrey,
  }
});

export default styles;
