import axios from "axios";
import React, {useEffect, useState} from "react";
import { ActivityIndicator, Text, View, SafeAreaView, FlatList, Image } from "react-native";

import styles from "./styles";


export type Pony = {
  id: number;
  name: string;
  alias: string;
  url: string;
  sex: string;
  residence: string;
  occupation: string;
  kind?: string[] | null;
  image?: string[] | null;
};

export type FetchDataProps = {
  showSpinner: boolean;
}

const ScreenThree = () => {
  const [ponies, setPonies] = useState<Pony[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [loadingMore, setLoadingMore] = useState<boolean>(false);
  const [offset, setOffset] = useState<number>(0);


  const handleEndReached = () => {
    setOffset(updatedOffset => updatedOffset + 10);
  }
  const renderItem = ({item}: {item: Pony}) => {
    const { name, image, alias, sex } = item;
    const imageUrl = image ? image[0] : '';

    return (
      <View style={styles.item}>
        <Image 
          style={styles.image} 
          source={{uri: imageUrl}}
          resizeMethod="auto"
        />
        <Text style={styles.itemTextPrimary}>{name}</Text>
        {alias && <Text style={styles.itemTextSecondary}>{alias}</Text>}
        <Text style={styles.itemTextSecondary}>{sex}</Text>
      </View>
    )
  }

  const renderContent = () => {
    if (loading) {
      return (
        <ActivityIndicator 
          size="large"
          style={styles.activityIndicator}
        />
      );
    } else {
      return (
        <SafeAreaView style={styles.container}>
          <Text style={styles.title}>My Little Ponies</Text>
          <FlatList
            keyExtractor={item => `${item.id}${Math.random()}`}
            data={ponies}
            renderItem={renderItem}
            numColumns={2}
            onEndReached={handleEndReached}
            onEndReachedThreshold={0.1}
          />  
        </SafeAreaView>
      );
    }
  };

  const setPonyData = (ponyData: Pony[]) => {
    setLoading(false);
    setLoadingMore(false);
    setPonies([...ponies, ...ponyData]);
  }

  const fetchData = async ({showSpinner}: FetchDataProps) => {
    let ponyData: Pony[] = [];

    showSpinner ? setLoading(true) : setLoadingMore(true);

    try {
      const url = `https://ponyweb.ml/v1/character/all?limit=10&offset=${offset}`
      const response = await axios(url);

      const {data} = response || {};
      ponyData = data?.data;
    } catch(error) {
      console.log('failed to fetch');
    }

    if (ponyData.length > 0) {
      setPonyData(ponyData);
    }
  }

  useEffect(() => {
    if (offset > 0 && !loadingMore) {
      fetchData({showSpinner: false});
    }
  }, [offset]);

  useEffect(() => {
    fetchData({showSpinner: true});
  }, []);

  return renderContent();
};

export default ScreenThree;
