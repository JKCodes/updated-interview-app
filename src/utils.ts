export const Colors = {
  purple: "#912EAB",
  darkGrey: "#999",
  grey: "#EEEEEE",
  lightGrey: "#F7F7F7",
  pink: "#F140A9",
  blue: "#35ABED",
  white: "white",
  black: "black",
};

export const SHADOW_STYLE = {
  shadowColor: "black",
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.05,
};
