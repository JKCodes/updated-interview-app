import React, {useState, useEffect} from "react";
import { FlatList, StatusBar, Text, TextInput, View, TouchableOpacity } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage'; 
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faCheck, faMinusCircle } from "@fortawesome/free-solid-svg-icons";
import { Colors } from "../utils";
import styles from "./styles";

type ItemType = {
  id: string;
  text: string;
  isFinished: boolean;
}

type RenderItemType = {
  item: ItemType,
  index: number;
}

const ScreenTwo = () => {
  const [title, setTitle] = useState<string>('Start Timer');
  const [counter, setCounter] = useState<number>(0);
  const [items, setItems] = useState<ItemType[]>([]);
  const [text, setText] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(true);

  let timer: NodeJS.Timeout | undefined;

  const handleAddItem = () => {
    if (!text) return;

    const item = {
      id: `${Math.random()}`,
      text,
      isFinished: false,
    }

    setItems([
      ...items,
      item
    ]);
    setText('');
  };

  const handleChangeText = (text: string) => {
    setText(text);
  };

  const handleItemTap = (index: number) => {  
    const item: ItemType = {
      ...items[index],
      isFinished: !items[index].isFinished
    }
    const updatedItems = [
      ...items.slice(0, index),
      item,
      ...items.slice(index + 1)
    ];

    setItems(updatedItems);
  };

  const handleMinusTap = (index: number) => {
    const updatedItems = [
      ...items.slice(0, index),
      ...items.slice(index + 1)
    ];

    setItems(updatedItems);
  }

  const renderItem = ({item, index}: RenderItemType) => (
    <TouchableOpacity 
      style={styles.item} 
      key={Math.random()}
      onPress={() => handleItemTap(index)}  
    >
      {item.isFinished && <FontAwesomeIcon icon={faCheck} size={15} color={Colors.blue} />}
      <Text>{item.text}</Text>
      <TouchableOpacity onPress={() => handleMinusTap(index)}>
        <FontAwesomeIcon
          icon={faMinusCircle}
          size={15}
          color={Colors.pink}
        />
        </TouchableOpacity>
    </TouchableOpacity>
  );

  const startTimer = () => {
    timer = setInterval(() => {
      setCounter(counter => counter + 1);
    }, 1000);
  }

  const fetchToDo = async () => {
    try {
      const todos = await AsyncStorage.getItem('hello_todo');

      if (todos) {
        setItems(JSON.parse(todos));
      }
      setLoading(false);
    } catch (e) {
      console.log('failed to persist todo');
    }
  }

  const persistTodo = async (updatedItems: ItemType[]) => {
    try {
      await AsyncStorage.setItem('hello_todo', JSON.stringify(updatedItems));
    } catch (e) {
      console.log('failed to persist todo');
    }
  }

  useEffect(() => {
    if (loading) return;
  
    persistTodo(items);
  }, [items, loading])

  useEffect(() => {
    setTitle('Timer: ');

    fetchToDo();
    startTimer();

    return () => {
      if (timer) {
        clearInterval(timer);
      }
    }
  }, [])


  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        {title} {counter}
      </Text>

      <View style={styles.headerContainer}>
        <Text style={styles.title}>
          Add New To-Do {StatusBar.currentHeight}
        </Text>
        <TextInput
          style={styles.input}
          value={text}
          placeholder="Enter To Do"
          onChangeText={handleChangeText}
          onSubmitEditing={handleAddItem}
        />
        <TouchableOpacity style={styles.submitToDoButton} onPress={handleAddItem}>
          <Text style={styles.buttonTitle}>Add To-Do</Text>
        </TouchableOpacity>
      </View>

      <FlatList
        style={styles.flatList}
        data={items}
        keyExtractor={item => item.id}
        renderItem={renderItem}
      />
    </View>
  );
};

export default ScreenTwo;
