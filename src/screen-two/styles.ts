import { StyleSheet } from "react-native";
import { Colors, SHADOW_STYLE } from "../utils";

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.grey,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 200,
  },
  headerContainer: {
    padding: 14,
    width: "90%",
    marginVertical: 20,
    backgroundColor: "white",
    borderRadius: 10,
    ...SHADOW_STYLE,
  },
  input: {
    marginTop: 20,
    backgroundColor: Colors.lightGrey,
    borderRadius: 6,
    padding: 10,
  },
  submitToDoButton: {
    marginTop: 20,
    height: 40,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Colors.purple,
  },
  buttonBackground: {
    backgroundColor: Colors.blue,
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    borderRadius: 10,
    marginTop: 20,
  },
  item: {
    padding: 16,
    width: '100%',
    borderRadius: 5,
    marginBottom: 5,
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between",
    ...SHADOW_STYLE,
  },
  title: {
    fontSize: 22,
    fontWeight: "600",
  },
  buttonTitle: {
    color: "white",
    fontSize: 18,
    fontWeight: "600",
  },
  flatList: {
    width: '90%',
  }
});

export default styles;
