import React, { useCallback } from "react";
import { View } from "react-native";
import TabBarButton from "../tab-bar-button";
import styles from "./styles";

export type ButtonData = {
  label: string;
  number: number;
  screen?: JSX.Element;
  onPress?: (number: number) => void;
};

const TabBar = ({
  buttons,
  selected,
  onPress,
}: {
  buttons: ButtonData[];
  selected: number;
  onPress: (number: number) => void;
}) => {
  const handlePress = useCallback(
    (number: number) => {
      onPress(number);
    },
    [onPress],
  );
  return (
    <View style={styles.container}>
      {buttons.map((button) => {
        return (
          <TabBarButton
            {...button}
            onPress={handlePress}
            selected={selected === button.number}
          />
        );
      })}
    </View>
  );
};
export default TabBar;
