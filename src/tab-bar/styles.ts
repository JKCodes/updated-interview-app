import { StyleSheet } from "react-native";
import { SHADOW_STYLE } from "../utils";

const BAR_HEIGHT = 50;

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    borderRadius: 8,
    marginHorizontal: 14,
    backgroundColor: "white",
    height: BAR_HEIGHT,
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 30,
    ...SHADOW_STYLE,
  },
});
export default styles;
