import React, { useCallback, useState } from "react";
import { View } from "react-native";
import ScreenOne from "../screen-one";
import ScreenTwo from "../screen-two";
import ScreenThree from "../screen-three";
import TabBar, { ButtonData } from "../tab-bar";
import styles from "./styles";

const BUTTONS: ButtonData[] = [
  { label: "One", number: 1 },
  { label: "Two", number: 2 },
  { label: "Three", number: 3 },
];

const App = () => {
  const [screen, setScreen] = useState(1);
  const handlePress = useCallback((screenNumber: number) => {
    setScreen(screenNumber);
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.container}>
        {screen === 1 ? <ScreenOne /> : null}
        {screen === 2 ? <ScreenTwo /> : null}
        {screen === 3 ? <ScreenThree /> : null}
      </View>
      <TabBar buttons={BUTTONS} onPress={handlePress} selected={screen} />
    </View>
  );
};

export default App;
