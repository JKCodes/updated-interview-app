# TechStyle Interview App

## Contents

  - [Setup Instructions](#setup-instructions)
    - [Software](#software)
    - [Project Clone, Setup and Running for the First Time](#project-clone-setup-and-running-for-the-first-time)
      - [More Detailed Running Instructions](#more-detailed-running-instructions)
  - [What we're looking for](#what-were-looking-for)
  - [Goals](#goals)
    - [Screen One](#screen-one)
    - [Screen Two](#screen-two)
    - [Screen Three](#screen-three)
    - [Stretch Goals](#stretch-goals)
## Setup Instructions

### Software

Before you follow the remaining instructions, ensure you have the following installed on your mac:

- An up-to-date version of `Xcode` with the command line tools added
  - See the [react native setup docs](https://reactnative.dev/docs/environment-setup) and select the `React Native CLI Quickstart` tab for more details on this step
- `Yarn`: [Yarn setup](https://yarnpkg.com/lang/en/docs/install/#mac-stable)
- [`Cocoapods`](https://cocoapods.org/): in your terminal, run `sudo gem install cocoapods`
- Watchman `brew install watchman`
- [`Node`](https://nodejs.org/en/) (latest LTS version) e.g. via [NVM](https://github.com/nvm-sh/nvm) or `brew install node`

### Project Clone, Setup and Running for the First Time

1. Clone this repo via SSH and open the directory: `git clone git@bitbucket.org:techstyleinc/react-native-interview-app.git && cd react-native-interview-app`
2. Run `yarn install:fresh`, which does the following for you:
   1. Runs `yarn` to install the project's dependencies
   2. Runs `(cd ios && pod install)` to set up pods while keeping you in the root directory of the project
3. Run `yarn start` to start the `react-native Metro Bundler`
4. Run `yarn ios` in a new terminal tab/window to compile the app and open the simulator
   - If this doesn't work, see the more detailed instructions below
5. If you run into issues when it comes to pushing to the this repo, check the [bitbucket common issue troubleshooting page](https://confluence.atlassian.com/bitbucket/troubleshoot-ssh-issues-271943403.html)

#### More Detailed Running Instructions

Use one of the following options to compile the app and run the simulator:

1. Using `Xcode`'s GUI:
   - In `Xcode`, open the project workspace (`ios/interview.xcworkspace`)
   - Build (press the "play" button (`â–¶`) in the top left of the window)
2. Without `Xcode`'s GUI:
   - Install the React Native cli globally: `yarn global add react-native-cli`
   - While metro bundler is active, run `yarn ios` in the project root (the `interview-app` directory) in a new terminal window/tab
   - You can specify the simulator to use with the `--simulator` flag, e.g. `--simulator="iPhone X"`
   - You can specify to run the app on your phone with `--device="My iPhone"` where "My iPhone" is your device name. You can find more information in the [official docs](https://reactnative.dev/docs/running-on-device)

## What we're looking for

Our main goal with this sample project is to get a better understanding of if you'll be a good fit for our team, and project. We'll be looking for these main attributes on no particular order:

- React Native competency
- Problem solving skills
- Your ability to communicate your problems and ideas
- How you use the tools available to you
- How well you handle given requirements
- Reading comprehension
- If you like shorts, and how short specifically
- And more!...

Treat this like a work environment. Use the internet, source documentation, ask questions to the team.

## Goals

Over the course of the next hour, we'll be working towards completing as many of the following goals as you can. Before you dive into each Screen's goals, please read them all. It could sway how you think about solving the problems more efficiently.

We've also provided some stretch goals if you're able to complete all initial goals.

### Screen One

In this first screen, there are 2 main goals.

1. A new To-Do should be added when the user both taps the 'Add To-Do' button and when they hit the 'Enter' key on the keyboard after typing a new To-Do.
2. Add the ability to mark a To-Do as complete. We've added a checkmark into the component for easy access, but you're more than welcome to modify its styling. Tapping on the whole cell should mark it as complete, and the UI should clearly show that it is done.
3. Fix Bugs as many bugs as you can find.


### Screen Two

This one's pretty simple:

1. Duplicate all functionality from Screen One, but in a Functional Component.

> Note: use of hooks is especially important here. If you're unfamiliar with hooks, you can refer to the React documentation [here.](https://reactjs.org/docs/hooks-intro.html)

### Screen Three

Here, we're looking to see how you handle and manage data from a remote server.

1. Build a screen that matches the mock provided in `screen-three.png` (Also shown below). You won't be provided with specific margins, fonts, etc. There is a list of available colors in the utils file. Simply do your best to get as close to the mock as you can. However, be mindful of where you spend your time.
2. Fetch data from `https://ponyweb.ml/v1/character/all` to populate each row. The data model it supplies should be self explainatory.
3. Consider loading states, as we still want a good user experience.

![Screen Three Mock](./src/screen-three/screen-three.png "Screen Three Mock")

### Stretch Goals

These tasks are intended to be completed after you're done with all previous goals.

1. The Tab Bar doesn't show which tab is selected. Add some UI so that it's clear which tab is open, and which ones aren't.
2. Add unit tests via jest _somewhere_ in the app.
3. **One or Two** - A To-Do should be created when they hit the 'enter' button on the software keyboard.
4. **One or Two** - Add the ability to delete a To-Do from the list. A delete icon has been added for easy access.
5. **One or Two** - Add support for persisting To-Do's. Adding a to do, should be stored globally _somewhere_. Then when the screen re-mounts, we should pull active To-Do's from that storage.
6. Implement pagination in Screen Three. When the screen mounts, only request the first 10 ponies. Then when the user scrolls to the bottom of the list, request the next 10.
